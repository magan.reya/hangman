hangman
====

This project implements the game of Breakout.

Name: Reya Magan

### Timeline

Start Date: 9/4

Finish Date: 9/7

Hours Spent: 20


### Tutorial and other Resources Used
https://courses.cs.duke.edu/compsci101/spring14/assign/05_hangman/howto.php#Clever,  
https://courses.cs.duke.edu/fall16/compsci101/assign/assign7-cleverhangman/ - To understand clever players

https://www.geeksforgeeks.org/randomly-select-items-from-a-list-in-java/ - Random Selection

https://www.dummies.com/programming/java/how-to-create-lines-and-shapes-in-javafx/ - javaFX shapes

https://www.geeksforgeeks.org/java-program-to-count-the-occurrence-of-each-character-in-a-string-using-hashmap/ - count characters in list



### Resource Attributions
My testwords.txt file is taken from the example in the above CS101 assignment. Otherwise I did not use any other resources besides the links above.

### Running the Program

Main class: Main.java

Data files needed: You can use testwords.txt, lowerwords.txt, or movies.txt

Key/Mouse inputs: You will need to enter the file to use (DICTIONARY), the letter limit for the word to be guessed (NUM_LETTERS), and the type of guesser/keeper ("INTERACTIVE", "CLEVER", "SIMPLE")

Known Bugs: For the Clever Keeper vs Simple Guesser game, the whole game does run (and the person is drawn), but instead of ending at a game over screen I keep getting an index out of bounds error. This is probably a simple bug, but I was unable to pinpoint the issue. The Clever Keeper vs Clever Guesser returns the same final word each time, which I thought was a bug but this makes sense since both players can "see" the words list.


### Notes/Assumptions
This game is only configured to take 8 guesses (the person won't draw more than that). I wanted to implement more, but simply did not have enough time in the end. Also, in terms of the clever players, I am assuming my understanding was correct, but it could have some gaps. For the keeper, I change my secret word each time depending on whether the guessed letter shows up in more possible words than it does not, and I always guess the most common letter in the remaining words for the Clever Guesser.


### Impressions
This project was a lot of fun, but I found it really difficult as well. It took me a really long time to visualize how a clever player worked, and because of this I didn't have much time to add more visual features to the person in the scene. Still, it was a good challenge and I got to practice more toward writing readable and organized code. 

