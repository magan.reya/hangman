package hangman;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;
import util.DisplayWord;
import util.HangmanDictionary;

public class HangmanGame {
    // constant
    public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    private static final String LETTERS_ORDERED_BY_FREQUENCY = "etaoinshrldcumfpgwybvkxjqz";


    private String mySecretWord;
    private int myNumGuessesLeft;
    private DisplayWord myDisplayWord;
    private StringBuilder myLettersLeftToGuess;
    private String myGuess;
    private String myLetters;
    private int myIndex;
    private String myKeeperType;
    private String myGuesserType;
    private ArrayList<String> myPossibleWords;
    private DisplayWord tempDisplay;
    private HashMap<DisplayWord, List<String>> myMatchingWords = new HashMap<>();
    private int myWordLength;
    private ArrayList<String> myCurrentPossibleWords;
    private ArrayList<Node> myVisualElements;
    private ArrayList<Node> myGallowsElements;
    private int livesLostCount = 0;
    private int guesses;

    // JFX variables
    private Scene myScene;
    private Timeline myAnimation;
    private List<Text> mySecretWordDisplay;
    private List<Text> myLettersLeftToGuessDisplay;
    private Group root = new Group();


    Guesser guess = new Guesser();
    SecretKeeper keeper = new SecretKeeper();
    HangmanVisual visual = new HangmanVisual();
    Clever clever = new Clever();

    public HangmanGame(String keeperType, String guesserType, HangmanDictionary dictionary, int wordLength, int numGuesses) {
        myNumGuessesLeft = numGuesses;
        guesses = numGuesses;
        myLettersLeftToGuess = new StringBuilder(ALPHABET);
        myKeeperType = keeperType;
        myGuesserType = guesserType;
        mySecretWord = secretWordSet(dictionary, wordLength);
        myDisplayWord = new DisplayWord(mySecretWord);
        myLetters = LETTERS_ORDERED_BY_FREQUENCY;
        myIndex = 0;
        myPossibleWords = (ArrayList<String>) dictionary.getWords(wordLength);
        myWordLength = wordLength;
    }

    public String secretWordSet(HangmanDictionary dictionary, int wordLength){
        String secretWord = new String();
        if (myKeeperType == "INTERACTIVE") {
            secretWord = keeper.getInput(String.format("Please enter a secret word %d letters long", wordLength), wordLength);
        }
        if (myKeeperType == "SIMPLE" || myKeeperType == "CLEVER") {
            secretWord = keeper.setInput(dictionary, wordLength);
        }
        return secretWord;
    }
    /**
     * Start the game by animating the display of changes in the GUI every speed seconds.
     */
    public void start(double speed) {
        myAnimation = new Timeline();
        myAnimation.setCycleCount(Timeline.INDEFINITE);
        myAnimation.getKeyFrames().add(new KeyFrame(Duration.seconds(speed), e -> getGuess(myPossibleWords)));
        myAnimation.play();
    }

    public void setupVisual() {
        myGallowsElements = visual.setupGallowsElements();
        for(int k = 0; k < myGallowsElements.size(); k ++){
            root.getChildren().add(myGallowsElements.get(k));
        }
        myVisualElements = visual.setupGuessElements();
    }

    public void drawPerson(int index) {
        System.out.println(livesLostCount);
        root.getChildren().add(myVisualElements.get(index));
    }

    public Scene setupDisplay(int width, int height, Paint background) {
        // show letters available for guessing
        setupVisual();
        myLettersLeftToGuessDisplay = new ArrayList<>();
        for (int k = 0; k < ALPHABET.length(); k += 1) {
            Text displayLetter = new Text(50 + 20 * k, 50, ALPHABET.substring(k, k + 1));
            displayLetter.setFont(Font.font("Arial", FontWeight.BOLD, 20));
            myLettersLeftToGuessDisplay.add(displayLetter);
            root.getChildren().add(displayLetter);
        }
        // show word being guessed, with letters hidden until they are guessed
        mySecretWordDisplay = new ArrayList<>();
        for (int k = 0; k < myDisplayWord.toString().length(); k += 1) {
            Text displayLetter = new Text(200 + 20 * k, 500, myDisplayWord.toString().substring(k, k + 1));
            displayLetter.setFont(Font.font("Arial", FontWeight.BOLD, 40));
            mySecretWordDisplay.add(displayLetter);
            root.getChildren().add(displayLetter);
        }
        // create place to see and interact with the shapes
        myScene = new Scene(root, width, height, background);
        if (myGuesserType == "INTERACTIVE") {
            myScene.setOnKeyPressed(e -> myGuess = guess.handleKeyInput(e.getCode()));

        }
        return myScene;
    }

    public void playGuess(ArrayList<String> wordsList) {
        // has user guessed?
        if (myGuess == null) {
            return;
        }
        // handle only valid guesses
        if (myGuess.length() == 1 && ALPHABET.contains(myGuess)) {
            int index = myLettersLeftToGuess.indexOf(myGuess);
            // do not count repeated guess as a miss
            if (index < 0) {
                return;
            }
            // record guess
            myLettersLeftToGuess.setCharAt(index, ' ');
            checkKeeper(wordsList);
            // update letters displayed to the user
            for (int k = 0; k < myLettersLeftToGuess.length(); k += 1) {
                myLettersLeftToGuessDisplay.get(k).setText(myLettersLeftToGuess.substring(k, k + 1));
            }
            for (int k = 0; k < myDisplayWord.toString().length(); k += 1) {
                mySecretWordDisplay.get(k).setText(myDisplayWord.toString().substring(k, k + 1));
            }
        } else {
            System.out.println("Please enter a single alphabetic letter ...");
        }
        myGuess = null;
        // check for end of game
        checkEnd();
    }

    public void checkEnd(){
        if (myNumGuessesLeft == 0) {
            System.out.println("YOU ARE HUNG!!!");
            myScene.setOnKeyPressed(null);
            myAnimation.stop();
        } else if (myDisplayWord.equals(mySecretWord)) {
            System.out.println("YOU WIN!!!");
            myScene.setOnKeyPressed(null);
            myAnimation.stop();
        }
    }

    public void checkKeeper(ArrayList<String> wordsList){
        if (!mySecretWord.contains(myGuess) && myKeeperType != "CLEVER") {
            myNumGuessesLeft -= 1;
            drawPerson(livesLostCount);
            livesLostCount++;
        } else if (myKeeperType != "CLEVER") {
            myDisplayWord.update(myGuess.charAt(0), mySecretWord);
        }
        if (myKeeperType == "CLEVER") {
            myMatchingWords = clever.cleverChecks(wordsList, myGuess);
            myCurrentPossibleWords = clever.findMaxClever();
            mySecretWord = clever.newSecretWord(myCurrentPossibleWords);
            cleverDisplayWord();
        }

    }
    public void getGuess(ArrayList<String> myWordsList) {
        myGuess = guess.getMyGuess(myGuesserType, myWordsList, myLetters, myIndex);
        myIndex++;
        playGuess(myWordsList);
    }

    public void cleverDisplayWord() {
        if (!mySecretWord.contains(myGuess)) {
            myNumGuessesLeft--;
            if (livesLostCount < guesses) {
                drawPerson(livesLostCount);
                livesLostCount++;
            }

        }
        myDisplayWord.update(myGuess.charAt(0), mySecretWord);
        myPossibleWords = myCurrentPossibleWords;
        getGuess(myPossibleWords);
    }
}

