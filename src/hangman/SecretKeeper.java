package hangman;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;


import javafx.scene.control.TextInputDialog;
import util.HangmanDictionary;


public class SecretKeeper {
    public String mySecretWord;

    public String setInput(HangmanDictionary dictionary, int numCharacters){
        mySecretWord = dictionary.getRandomWord(numCharacters).toLowerCase();
        return mySecretWord;
    }
    // Get user's text input to use as the secret word
    public String getInput (String prompt, int numCharacters) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setContentText(prompt);
        // DO NOT USE IN GENERAL - this is a TERRIBLE solution from UX design, and we will better ways later
        Optional<String> answer = dialog.showAndWait();
        while (answer.isEmpty() || answer.get().length() != numCharacters) {
            answer = dialog.showAndWait();
        }
        return answer.get();
    }
    public String setNewWordClever(ArrayList<String> listOfWords){
        Random rand = new Random();
        return listOfWords.get(rand.nextInt(listOfWords.size()));
    }
}
