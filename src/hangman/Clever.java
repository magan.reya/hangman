package hangman;

import util.DisplayWord;

import java.util.*;

public class Clever {
    private HashMap<DisplayWord, List<String>> myMatchingWords = new HashMap<>();
    private DisplayWord tempDisplay;
    private ArrayList<String> myCurrentPossibleWords;

    SecretKeeper keeper = new SecretKeeper();

    public HashMap<DisplayWord, List<String>> cleverChecks(ArrayList<String> myWords, String myGuess){
        myMatchingWords.clear();

        for(int k = 0; k < myWords.size(); k++){
            tempDisplay = new DisplayWord(myWords.get(k));
            tempDisplay.update(myGuess.charAt(0), myWords.get(k));

            if(!myMatchingWords.containsKey(tempDisplay)){
                myMatchingWords.put(tempDisplay, new ArrayList<String>());
            }
            if(myMatchingWords.containsKey(tempDisplay)){
                myMatchingWords.get(tempDisplay).add(myWords.get(k));
            }
        }
        findMaxClever();
        return myMatchingWords;
    }
    public ArrayList<String> findMaxClever(){
        Iterator<Map.Entry<DisplayWord, List<String>>> iter = myMatchingWords.entrySet().iterator();
        int maxVal = 0;
        for(DisplayWord j : myMatchingWords.keySet()){
            if(myMatchingWords.get(j).size() > maxVal){
                maxVal = myMatchingWords.get(j).size();
            }
        }
        while(iter.hasNext()){
            Map.Entry<DisplayWord, List<String>> entry = iter.next();
            if(entry.getValue().size() < maxVal ){
                iter.remove();
            }
        }
        for(DisplayWord i: myMatchingWords.keySet()){
            myCurrentPossibleWords = (ArrayList<String>) myMatchingWords.get(i);
        }
        return myCurrentPossibleWords;
    }
    public String newSecretWord(ArrayList<String> currentWords){
        String mySecretWord = keeper.setNewWordClever(currentWords);
        return mySecretWord;
    }



}
