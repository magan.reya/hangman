package hangman;

import javafx.scene.input.KeyCode;

import java.util.*;


public class Guesser {

    public String myGuess;
    public HashMap<Character, Integer> characterCount = new HashMap<>();
    public ArrayList<Character> usedCharacters = new ArrayList<>();

    public String getMyGuess(String guessType, ArrayList<String> myWordsList, String myLetters, int myIndex){
        if (guessType == "SIMPLE") {
            myGuess = makeGuess(myLetters, myIndex);
        }
        if (guessType == "CLEVER") {
            myGuess = makeCleverGuess(myWordsList);
        }
        return myGuess;
    }

    public String makeGuess(String letters, int index) {
        myGuess = "" + letters.charAt(index);
        return myGuess;
    }

    public String handleKeyInput (KeyCode code) {
        myGuess = code.getChar().toLowerCase();

        System.out.println("*** " + myGuess);
        return myGuess;

    }
    public String makeCleverGuess(ArrayList<String> myWords){
        characterCount.clear();
        System.out.println(myWords);
        for(String s: myWords){
            char[] j = s.toCharArray();
            for(char c: j) {
                if(usedCharacters.contains(c)){
                    characterCount.put(c, 0);
                }
                if (!usedCharacters.contains(c)) {
                    if (!characterCount.containsKey(c)) {
                        characterCount.put(c, 1);
                    } else {
                        characterCount.put(c, characterCount.get(c) + 1);
                    }
                }
            }
        }

        myGuess = getCleverGuess();
        return myGuess;
    }
    public String getCleverGuess(){
        int maxNum = 0;
        String maxCharacter = null;
        Character maxChar = null;

        for(char c: characterCount.keySet()) {
            if (characterCount.get(c) > maxNum) {
                maxNum = characterCount.get(c);
                maxCharacter = String.valueOf(c);
                maxChar = c;
            }
            if (!usedCharacters.contains(maxCharacter)) {
                myGuess = maxCharacter;

            }
        }
        usedCharacters.add(maxChar);
        System.out.println(characterCount);
        System.out.println(myGuess);
        return myGuess;

    }
}


