package hangman;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;

import java.util.ArrayList;



public class HangmanVisual {
    public static final int HANGING_X = 275;
    public static final int HANGING_LARGE_INITIAL_Y = 200;
    public static final int HANGING_Y_INITIAL = 200;
    public static final int HANGING_Y_FINAL = 225;
    public static final Color BLACK = Color.BLACK;
    public static final int TOP_INITIAL_X = 275;
    public static final int TOP_Y = 200;
    public static final int TOP_FINAL_X = 350;
    public static final int HANGING_LARGE_X = 350;
    public static final int HANGING_LARGE_FINAL_Y = 400;
    public static final int BOTTOM_INITIAL_X = 300;
    public static final int BOTTOM_Y = 400;
    public static final int BOTTOM_FINAL_X = 400;

    public static final int HEAD_RADIUS = 15;
    public static final int HEAD_X = 275;
    public static final int HEAD_Y = 226;
    public static final int BODY_INTERSECTIONS = 275;
    public static final int BODY_START = 241;
    public static final int BODY_END = 291;
    public static final int LEFT_ARM_Y_INIT = 261;
    public static final int LEFT_ARM_X_FIN = 250;
    public static final int LEFT_ARM_Y_FIN = 241;
    public static final int RIGHT_ARM_Y_INIT = 261;
    public static final int RIGHT_ARM_X_FIN = 300;
    public static final int RIGHT_ARM_Y_FIN = 241;
    public static final int LEG_START = 291;
    public static final int LEFT_LEG_X_FIN = 250;
    public static final int LEG_END = 311;
    public static final int RIGHT_LEG_X_FIN = 300;
    public static final int SHOE_X_RADIUS = 7;
    public static final int SHOE_Y_RADIUS = 3;
    public static final int LEFT_SHOE_X = 243;
    public static final int LEFT_SHOE_Y = 311;
    public static final int RIGHT_SHOE_X = 307;
    public static final int RIGHT_SHOE_Y = 311;
    public static final Color BLUE = Color.BLUE;
    public static final int EYE_RADIUS = 2;
    public static final int LEFT_EYE_X = 267;
    public static final int EYE_Y = 226;
    public static final int RIGHT_EYE_X = 283;
    public static final Color WHITE = Color.WHITE;
    public static final int MOUTH_X_RADIUS = 8;
    public static final int MOUTH_Y_RADIUS = 2;
    public static final int MOUTH_X = 275;
    public static final int MOUTH_Y = 235;
    public static final Color AQUA = Color.AQUA;

    ArrayList<Node> myVisuals = new ArrayList<Node>();
    ArrayList<Node> myGallows = new ArrayList<Node>();
    public Line setupLines(int x1, int y1, int x2, int y2, Paint fill, Boolean isBody){
        Line myLine = new Line(x1, y1, x2, y2);
        myLine.setStroke(fill);
        if(isBody){
            myVisuals.add(myLine);
        }
        else{
            myGallows.add(myLine);
        }
        return myLine;
    }
    public ArrayList<Node> setupGallowsElements(){
        setupLines(HANGING_X, HANGING_Y_INITIAL, HANGING_X, HANGING_Y_FINAL, BLACK, false);
        setupLines(TOP_INITIAL_X, TOP_Y, TOP_FINAL_X, TOP_Y, BLACK, false);
        setupLines(HANGING_LARGE_X, HANGING_LARGE_INITIAL_Y, HANGING_LARGE_X, HANGING_LARGE_FINAL_Y, BLACK, false);
        setupLines(BOTTOM_INITIAL_X, BOTTOM_Y, BOTTOM_FINAL_X, BOTTOM_Y, BLACK, false);

        return myGallows;
    }
    public ArrayList<Node> setupGuessElements(){
        setupHeadAndFace(HEAD_RADIUS, HEAD_X, HEAD_Y, AQUA);
        setupLines(BODY_INTERSECTIONS, BODY_START, BODY_INTERSECTIONS, BODY_END, BLACK, true);
        setupLines(BODY_INTERSECTIONS, LEFT_ARM_Y_INIT, LEFT_ARM_X_FIN, LEFT_ARM_Y_FIN, BLACK, true);
        setupLines(BODY_INTERSECTIONS, RIGHT_ARM_Y_INIT, RIGHT_ARM_X_FIN, RIGHT_ARM_Y_FIN, BLACK, true);
        setupLines(BODY_INTERSECTIONS, LEG_START, LEFT_LEG_X_FIN, LEG_END, BLACK, true);
        setupLines(BODY_INTERSECTIONS, LEG_START, RIGHT_LEG_X_FIN, LEG_END, BLACK, true);
        setupEllipses(SHOE_X_RADIUS, SHOE_Y_RADIUS, LEFT_SHOE_X, LEFT_SHOE_Y, BLACK);
        setupEllipses(SHOE_X_RADIUS, SHOE_Y_RADIUS, RIGHT_SHOE_X, RIGHT_SHOE_Y, BLACK);
        setupHeadAndFace(EYE_RADIUS, LEFT_EYE_X, EYE_Y, BLACK);
        setupHeadAndFace(EYE_RADIUS, RIGHT_EYE_X, EYE_Y, BLACK);
        setupEllipses(MOUTH_X_RADIUS, MOUTH_Y_RADIUS, MOUTH_X, MOUTH_Y, BLACK);
    return myVisuals;

    }
    public void setupHeadAndFace(int radius, int x, int y, Paint fill){
        Circle head = new Circle(radius, fill);
        head.setCenterY(y);
        head.setCenterX(x);
        myVisuals.add(head);
    }

    public void setupEllipses(int xRadius, int yRadius, int x, int y, Paint fill){
        Ellipse shoe = new Ellipse(x, y, xRadius, yRadius);
        shoe.setStroke(fill);
        myVisuals.add(shoe);
    }

}
